import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/state_manager.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pretty_logger/pretty_logger.dart';
import 'dart:math' show cos, sqrt, asin;
import 'package:http/http.dart' as http;

class MapControll extends GetxController {
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Position? position;
  BitmapDescriptor bitmapDescriptor = BitmapDescriptor.defaultMarker;
  BitmapDescriptor secondBitmapDescriptor = BitmapDescriptor.defaultMarker;
  BitmapDescriptor thirdBitmapDescriptor = BitmapDescriptor.defaultMarker;

  //FOR GATTING ADDRESS
  final startAddressController = TextEditingController();

  List<LatLng>? result;
  Map<PolylineId, Polyline> polylines = {};

  double latitude = 0.0;
  double logitude = 0.0;
  double mapLatitude = 0.0;
  double mapLogitude = 0.0;
  double distanceMeter = 0.0;
  bool isGranted = false;
  PolylinePoints polylinePoints = PolylinePoints();
  PolylineResult? polyResult;
  List<LatLng> polylineCoordinates = [];
  double totalDistance = 0.0;
  String placeDistance = "";
  addPolyLine(polylineCoordinates) {
    PolylineId id = const PolylineId("value");
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.deepPurpleAccent,
      points: polylineCoordinates,
      width: 3,
    );
    polylines[id] = polyline;
    update();
  }

  Future<dynamic> getDistanceTime() async {
    String url =
        "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=${21.7771574},${87.729263}&origins=$latitude,$logitude&key=AIzaSyC_seZ-KPLTbzTFiJeL0KSJGixI28uC1Yw";
    // try {
    var responce = await http.get(Uri.parse(url));
    if (responce.statusCode == 200) {
      PLog.green(responce.body);
      return jsonDecode(responce.body);
    } else {
      return null;
    }
    // } catch (e) {
    //   PLog.red("This is error from getDistanceTime method");
    // }
  }

  getDirections(double lat, double long) async {
    // List<LatLng> polylineCoordinates = [];

    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      "AIzaSyC_seZ-KPLTbzTFiJeL0KSJGixI28uC1Yw",
      PointLatLng(lat, long),
      const PointLatLng(21.7771574, 87.729263),
      travelMode: TravelMode.driving,
    );

    if (result.points.isNotEmpty) {
      for (var point in result.points) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      }
    } else {
      print(result.errorMessage);
    }
    addPolyLine(polylineCoordinates);
  }

  Future<void> addMarkerIcon() async {
    bitmapDescriptor = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), "assets/images/flag.png");
    secondBitmapDescriptor = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), "assets/images/home.png");
    thirdBitmapDescriptor = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), "assets/images/cycle-lane.png");
    update();
  }

  StreamSubscription<ServiceStatus>? serviceStatusStream;
  StreamSubscription<Position>? positionStream;
  Future<void> streamSubcription() async {
    // serviceStatusStream =
    //     Geolocator.getServiceStatusStream().listen((ServiceStatus status) {
    //   PLog.green("status.toString()");

    //   update();
    //   PLog.green(positionStream!.asFuture().toString());
    // });

    positionStream = Geolocator.getPositionStream().listen((event) {
      latitude = event.latitude;
      logitude = event.longitude;

      PLog.red("This is position stream ${latitude}");
      update();
    });
    update();
  }

  Future<void> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    } else {
      PLog.red("Permission granted");
      position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      mapLatitude = position!.latitude;
      mapLogitude = position!.longitude;
      isGranted = true;
      update();
    }

    update();
  }

  void onMapCreated(GoogleMapController controller) {
    GoogleMapController mapController = controller;

    final marker = Marker(
      markerId: const MarkerId('place_name'),
      position: LatLng(position!.latitude, position!.longitude),
      icon: bitmapDescriptor,
      infoWindow: const InfoWindow(
        title: 'title',
        snippet: 'address',
      ),
    );

    markers[const MarkerId('place_name')] = marker;
    update();
  }

  // Position? currentPosition;
  // void distance() async {
  //   // currentPosition = await determinePosition();
  //   distanceMeter = Geolocator.distanceBetween(currentPosition!.latitude,
  //       currentPosition!.longitude, 22.5797323, 88.4351652);
  // }

  //FOR GETTING ADDRESS
  // Method for retrieving the address

  String currentAddress = "";
  String startAddress = "";
  String destinationAddress = "";
  String endAddress = "";
  Future<void> getAddress() async {
    // try {
    // Places are retrieved using the coordinates
    List<Placemark> p =
        await placemarkFromCoordinates(mapLatitude, mapLogitude);

    List<Placemark> e = await placemarkFromCoordinates(21.7771574, 87.729263);

    // Taking the most probable result
    Placemark place = p[0];
    Placemark place2 = e[0];

    // Structuring the address
    currentAddress =
        "${place.name}, ${place.locality}, ${place.postalCode}, ${place.country},${place.street}";
    destinationAddress =
        "${place2.locality}, ${place2.postalCode},${place2.country},${place2.administrativeArea},${place2.street}";

    // Update the text of the TextField
    // startAddressController.text = currentAddress;

    // Setting the user's present location as the starting address
    startAddress = currentAddress;
    endAddress = destinationAddress;
    update();
    PLog.green("this is start address $startAddress");
    PLog.green("this is start address $destinationAddress");
  }

  double coordinateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  double totalDistanceBetweenTwoCoordinate = 0.0;

// Calculating the total distance by adding the distance
// between small segments

  void distanceScale() {
    for (int i = 0; i < polylineCoordinates.length - 1; i++) {
      totalDistanceBetweenTwoCoordinate += coordinateDistance(
        polylineCoordinates[i].latitude,
        polylineCoordinates[i].longitude,
        polylineCoordinates[i + 1].latitude,
        polylineCoordinates[i + 1].longitude,
      );
    }

    placeDistance = totalDistanceBetweenTwoCoordinate.toStringAsFixed(2);
    PLog.green('DISTANCE: ${placeDistance} km');

    update();
  }
}
