import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:new_location/controller/map_control.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:geolocator/geolocator.dart';

class LocationPage extends StatefulWidget {
  const LocationPage({super.key});

  @override
  State<LocationPage> createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  MapControll mapControll = Get.find<MapControll>();
  Position? mapPosition;
  // Position mapPosition = Position(
  //     longitude: 0.0,
  //     latitude: 0.0,
  //     timestamp: DateTime.now(),
  //     accuracy: 100.0,
  //     altitude: 12.0,
  //     heading: 23.0,
  //     speed: 10.0,
  //     speedAccuracy: 13.0);

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      // await Geolocator.openAppSettings();
      // await Geolocator.openLocationSettings();
      await mapControll.determinePosition();

      await mapControll.getDirections(
          mapPosition!.latitude, mapPosition!.longitude);
      await mapControll.addMarkerIcon();

      await mapControll.streamSubcription();
      // polyline();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MapControll>(
      builder: (_) {
        return Scaffold(
          // backgroundColor: Colors.yellow,
          body: SingleChildScrollView(
            // physics: ClampingScrollPhysics(),
            child: StickyHeader(
                header: ClipRRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                        sigmaX: 30.0,
                        sigmaY: 30.0,
                        tileMode: TileMode.repeated),
                    child: Container(
                      color: Colors.black.withOpacity(0.3),
                      height: 150,
                      width: MediaQuery.sizeOf(context).width,
                      child: const Padding(
                        padding: EdgeInsets.only(top: 50),
                        child: Center(
                          child: Text(
                            "Track Order",
                            style: TextStyle(
                                letterSpacing: 1,
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                content: Flexible(
                  fit: FlexFit.tight,
                  child: SizedBox(
                    height: MediaQuery.sizeOf(context).height,
                    width: MediaQuery.sizeOf(context).width,
                    child: GoogleMap(
                      zoomGesturesEnabled: true,
                      tiltGesturesEnabled: false,
                      // minMaxZoomPreference: MinMaxZoomPreference(20, 20),
                      onCameraMove: (CameraPosition cameraPosition) {
                        mapControll.serviceStatusStream;
                      },
                      mapType: MapType.normal,
                      onMapCreated: (controller) {
                        mapControll.onMapCreated(controller);
                      },
                      myLocationEnabled: false,
                      initialCameraPosition: CameraPosition(
                        zoom: 15.0,
                        target: LatLng(
                            mapPosition!.latitude, mapPosition!.longitude),
                      ),
                      markers: {
                        Marker(
                          markerId: const MarkerId("demo"),
                          icon: mapControll.bitmapDescriptor,
                          position: LatLng(
                              mapControll.latitude, mapControll.logitude),
                          draggable: true,
                          onDrag: (value) {},
                        ),
                        Marker(
                          markerId: const MarkerId("destination"),
                          icon: mapControll.thirdBitmapDescriptor,
                          position: const LatLng(22.5797323, 88.4351652),
                          draggable: true,
                          onDrag: (value) {},
                        ),
                      },
                      polylines: Set<Polyline>.of(mapControll.polylines.values),
                    ),
                  ),
                )
                // : const SizedBox(
                //     height: 700,
                //     width: 500,
                //     child: Center(
                //       child: CircularProgressIndicator(),
                //     ),
                //   ),
                ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              showModalBottomSheet(
                isScrollControlled: true,
                clipBehavior: Clip.hardEdge,
                elevation: 0,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                context: context,
                builder: (BuildContext context) {
                  return SingleChildScrollView(
                    child: Container(
                      alignment: Alignment.bottomCenter,
                      // height: 656,
                      decoration: const BoxDecoration(
                          color: Colors.deepPurple,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20))),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const SizedBox(
                            height: 15,
                          ),
                          Row(
                            children: [
                              const Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 10)),
                              Container(
                                height: 70,
                                width: 70,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(120),
                                  color: Colors.white,
                                ),
                                child: Container(
                                  height: 65,
                                  width: 65,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(120),
                                    image: const DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                        "assets/images/man.jpg",
                                      ),
                                    ),
                                    // color: Colors.red,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Column(
                                children: [
                                  const Text(
                                    "Latoya Veum",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  Row(children: [
                                    Image.asset(
                                      "assets/images/star.jpeg",
                                      scale: 12,
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    const Text(
                                      "(4.7)",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ]),
                                ],
                              ),
                              const Spacer(),
                              Container(
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(120),
                                  color: Colors.white,
                                ),
                                child: SizedBox(
                                  height: 25,
                                  width: 25,
                                  child: Image.asset(
                                    "assets/images/chat.png",
                                    scale: 5,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              Container(
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(120),
                                  color: Colors.white,
                                ),
                                child: SizedBox(
                                  height: 25,
                                  width: 25,
                                  child: Image.asset(
                                    "assets/images/phone-call.png",
                                    scale: 5,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                            ],
                          ),
                          // const Spacer(),
                          const SizedBox(
                            height: 20,
                          ),
                          Container(
                            height: 550,
                            width: MediaQuery.sizeOf(context).width,
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                            ),
                            child: const Padding(
                              padding: EdgeInsets.only(left: 28.0),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "Shop",
                                      style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: SizedBox(
                                      width: 200,
                                      child: Text(
                                        "399 4th St,San Francisco,CA 94107,United State",
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "Current Location",
                                      style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: SizedBox(
                                      width: 200,
                                      child: Text(
                                        "399 4th St,San Francisco,CA 94107,United State",
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 50,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "Arriving in 10mins ",
                                      style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(right: 15.0),
                                    child: StepProgressIndicator(
                                      totalSteps: 100,
                                      currentStep: 32,
                                      size: 6,
                                      padding: 0,
                                      selectedColor: Colors.yellow,
                                      unselectedColor: Colors.cyan,
                                      roundedEdges: Radius.circular(10),
                                      selectedGradientColor: LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight,
                                        colors: [
                                          Colors.yellowAccent,
                                          Colors.deepOrange
                                        ],
                                      ),
                                      unselectedGradientColor: LinearGradient(
                                        begin: Alignment.topLeft,
                                        end: Alignment.bottomRight,
                                        colors: [Colors.black, Colors.blue],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: SizedBox(
                                      width: 200,
                                      child: Text(
                                        "To- 4th St,San Francisco,CA 94107,United State",
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "Item(s)",
                                          style: TextStyle(
                                              fontSize: 17,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 250,
                                      ),
                                      Text(
                                        "3item(s)",
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Row(
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: SizedBox(
                                          width: 200,
                                          child: Text(
                                            "Name",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.orange,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 50,
                                      ),
                                      Text(
                                        "Qyt.",
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.orange,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        width: 45,
                                      ),
                                      Text(
                                        "Price",
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.orange,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: SizedBox(
                                          width: 200,
                                          child: Text(
                                            "Chinmoy",
                                            style: TextStyle(
                                                fontSize: 10,
                                                color: Colors.grey,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 50,
                                      ),
                                      Text(
                                        "x1",
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        width: 55,
                                      ),
                                      Text(
                                        "\$75",
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "Google Pay",
                                      style: TextStyle(
                                          fontSize: 17,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      "\$175",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            },
          ),
        );
      },
    );
  }
}
