import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../controller/map_control.dart';

class MapViewPage extends StatefulWidget {
  const MapViewPage({super.key});

  @override
  State<MapViewPage> createState() => _MapViewPageState();
}

class _MapViewPageState extends State<MapViewPage> {
  MapControll mapControll = Get.find<MapControll>();

  Position? mapPosition;
  Map res = {};
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      // await Geolocator.openAppSettings();
      // await Geolocator.openLocationSettings();
      await mapControll.determinePosition();

      await mapControll.getDirections(
          mapControll.mapLatitude, mapControll.mapLogitude);
      await mapControll.addMarkerIcon();

      await mapControll.streamSubcription();
      // polyline();

      await mapControll.getAddress();
      mapControll.distanceScale();
      res = await mapControll.getDistanceTime();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MapControll>(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Google map integration"),
          centerTitle: true,
        ),
        body: mapControll.isGranted
            ? GoogleMap(
                zoomGesturesEnabled: true,
                tiltGesturesEnabled: false,
                // minMaxZoomPreference: MinMaxZoomPreference(20, 20),
                onCameraMove: (CameraPosition cameraPosition) {
                  mapControll.serviceStatusStream;
                },
                mapType: MapType.normal,
                onMapCreated: (controller) {
                  mapControll.onMapCreated(controller);
                },
                myLocationEnabled: false,
                initialCameraPosition: CameraPosition(
                  zoom: 15.0,
                  target:
                      LatLng(mapControll.mapLatitude, mapControll.mapLogitude),
                ),
                markers: {
                  Marker(
                    markerId: const MarkerId("demo"),
                    icon: mapControll.bitmapDescriptor,
                    position: LatLng(
                        mapControll.mapLatitude, mapControll.mapLogitude),
                    draggable: true,
                    onDrag: (value) {},
                  ),
                  Marker(
                    markerId: const MarkerId("vehicale"),
                    icon: mapControll.thirdBitmapDescriptor,
                    position:
                        LatLng(mapControll.latitude, mapControll.logitude),
                    draggable: true,
                    onDrag: (value) {},
                  ),
                  Marker(
                    markerId: const MarkerId("destination"),
                    icon: mapControll.secondBitmapDescriptor,
                    position: const LatLng(21.7771574, 87.729263),
                    draggable: true,
                    onDrag: (value) {},
                  ),
                },
                polylines: Set<Polyline>.of(mapControll.polylines.values),
              )
            : const SizedBox(
                height: 700,
                width: 500,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showModalBottomSheet(
              isScrollControlled: true,
              clipBehavior: Clip.hardEdge,
              elevation: 0,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              context: context,
              builder: (BuildContext context) {
                return SingleChildScrollView(
                  child: Container(
                    alignment: Alignment.bottomCenter,
                    // height: 656,
                    decoration: const BoxDecoration(
                        color: Colors.deepPurple,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20))),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const SizedBox(
                          height: 15,
                        ),
                        Row(
                          children: [
                            const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10)),
                            Container(
                              height: 70,
                              width: 70,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(120),
                                color: Colors.white,
                              ),
                              child: Container(
                                height: 65,
                                width: 65,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(120),
                                  image: const DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage(
                                      "assets/images/man.jpg",
                                    ),
                                  ),
                                  // color: Colors.red,
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              children: [
                                const Text(
                                  "Latoya Veum",
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                                Row(children: [
                                  Image.asset(
                                    "assets/images/star.jpeg",
                                    scale: 12,
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  const Text(
                                    "(4.7)",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ]),
                              ],
                            ),
                            const Spacer(),
                            Container(
                              height: 50,
                              width: 50,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(120),
                                color: Colors.white,
                              ),
                              child: SizedBox(
                                height: 25,
                                width: 25,
                                child: Image.asset(
                                  "assets/images/chat.png",
                                  scale: 5,
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Container(
                              height: 50,
                              width: 50,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(120),
                                color: Colors.white,
                              ),
                              child: SizedBox(
                                height: 25,
                                width: 25,
                                child: Image.asset(
                                  "assets/images/phone-call.png",
                                  scale: 5,
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                        // const Spacer(),
                        const SizedBox(
                          height: 20,
                        ),
                        Container(
                          height: 550,
                          width: MediaQuery.sizeOf(context).width,
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 28.0),
                            child: Column(
                              children: [
                                const SizedBox(
                                  height: 30,
                                ),
                                const Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Shop",
                                    style: TextStyle(
                                        fontSize: 17,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: SizedBox(
                                    width: 200,
                                    child: Text(
                                      mapControll.destinationAddress,
                                      style: const TextStyle(
                                          fontSize: 10,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                                const Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Current Location",
                                    style: TextStyle(
                                        fontSize: 17,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: SizedBox(
                                    width: 200,
                                    child: Text(
                                      mapControll.currentAddress,
                                      style: const TextStyle(
                                          fontSize: 10,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 50,
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    // "Arriving in ${mapControll.placeDistance} ",
                                    "Arriving in ${res['rows'][0]['elements'][0]["duration"]['text']}",
                                    style: const TextStyle(
                                        fontSize: 17,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(right: 15.0),
                                  child: StepProgressIndicator(
                                    totalSteps: 100,
                                    currentStep: 32,
                                    size: 6,
                                    padding: 0,
                                    selectedColor: Colors.yellow,
                                    unselectedColor: Colors.cyan,
                                    roundedEdges: Radius.circular(10),
                                    selectedGradientColor: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                      colors: [
                                        Colors.yellowAccent,
                                        Colors.deepOrange
                                      ],
                                    ),
                                    unselectedGradientColor: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                      colors: [Colors.black, Colors.blue],
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Align(
                                  alignment: Alignment.topLeft,
                                  child: SizedBox(
                                    width: 200,
                                    child: Text(
                                      "To- 4th St,San Francisco,CA 94107,United State",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                const Row(
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(
                                        "Item(s)",
                                        style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 250,
                                    ),
                                    Text(
                                      "3item(s)",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                const Row(
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: SizedBox(
                                        width: 200,
                                        child: Text(
                                          "Name",
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.orange,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 50,
                                    ),
                                    Text(
                                      "Qyt.",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.orange,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      width: 45,
                                    ),
                                    Text(
                                      "Price",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.orange,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Row(
                                  children: [
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: SizedBox(
                                        width: 200,
                                        child: Text(
                                          "Chinmoy",
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 50,
                                    ),
                                    Text(
                                      "x1",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      width: 55,
                                    ),
                                    Text(
                                      "\$75",
                                      style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                                const Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "Google Pay",
                                    style: TextStyle(
                                        fontSize: 17,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                const Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    "\$175",
                                    style: TextStyle(
                                        fontSize: 10,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          },
        ),
      );
    });
  }
}
